#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main()
{
    clock_t begin= clock();
    int r=1024,c=1024,i, j, k;
    int (*a)[1024] = calloc(r, sizeof *a);
    int (*b)[1024] = calloc(r, sizeof *b);
    int (*mul)[1024] = calloc(r, sizeof *mul);
    clock_t begin_rand = clock();
    // random
    for (i = 0; i < r; i++)
    {
        for (j = 0; j < c; j++)
        {
            a[i][j] = rand() % 100;
            
        }
    }
    for (i = 0; i < r; i++)
    {
        for (j = 0; j < c; j++)
        {
            b[i][j] = rand() % 100;
        }
    }
    clock_t end_rand = clock();
    double time_spent_rand = (double)(end_rand - begin_rand) / CLOCKS_PER_SEC;
    clock_t begin_calculate = clock();
    // calculate
    printf("Resultant matrix:\n");
    for (i = 0; i < r; i++)
    {
        for (j = 0; j < c; j++)
        {
            mul[i][j] = 0;
            for (k = 0; k < c; k++)
            {
                mul[i][j] += a[i][k] * b[k][j];
            }
        }
    }
    clock_t end_calculate = clock();
    double time_spent_calculate = (double)(end_calculate - begin_calculate) / CLOCKS_PER_SEC;
    clock_t begin_print = clock();
    // print result
    for (i = 0; i < r; i++)
    {
        for (j = 0; j < c; j++)
        {
            printf("%d\t", mul[i][j]);
        }
        printf("\n");
    }
    clock_t end_print = clock();
    double time_spent_print = (double)(end_print - begin_print) / CLOCKS_PER_SEC;
    free(a);
    free(b);
    free(mul);
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("Random number into matrix(1024*1024) took %f seconds to execute \n", time_spent_rand);
    printf("Calculate multipy matrix(1024*1024)  took %f seconds to execute \n", time_spent_calculate);
    printf("Printing matrix(1024*1024)           took %f seconds to execute \n", time_spent_print);
    printf("The program                          took %f seconds to execute \n", time_spent);
    return 0;
}
